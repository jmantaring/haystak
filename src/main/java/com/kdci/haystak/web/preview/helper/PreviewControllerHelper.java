package com.kdci.haystak.web.preview.helper;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface PreviewControllerHelper {

    void streamPreviewFile(HttpServletResponse response, String fileHash) throws IOException;

    void streamThumbnailFile(HttpServletResponse response, String fileHash) throws IOException;
}
