package com.kdci.haystak.web.preview;

import com.kdci.haystak.factory.FileHashDataBeanFactory;
import com.kdci.haystak.web.AbstractAjaxController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/preview")
public class PreviewAjaxController extends AbstractAjaxController {

    private FileHashDataBeanFactory fileHashDataBeanFactory; // NOTE: this might need to be pulled up to a helper

    @Autowired
    public void setFileHashDataBeanFactory(FileHashDataBeanFactory fileHashDataBeanFactory) {
        this.fileHashDataBeanFactory = fileHashDataBeanFactory;
    }

    @GetMapping("/setup/file/{fileHash}")
    @ResponseBody
    public Object setUp(@PathVariable("fileHash") String fileHash) {
        return fileHashDataBeanFactory.create(fileHash);
    }

}
