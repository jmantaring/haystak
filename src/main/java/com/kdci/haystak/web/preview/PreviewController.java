package com.kdci.haystak.web.preview;

import com.kdci.haystak.web.preview.helper.PreviewControllerHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/preview")
public class PreviewController {

    private PreviewControllerHelper previewControllerHelper;

    @Autowired
    public void setPreviewControllerHelper(PreviewControllerHelper previewControllerHelper) {
        this.previewControllerHelper = previewControllerHelper;
    }

    @GetMapping("/file/preview/{fileHash}")
    public void streamPreviewFile(@PathVariable("fileHash") final String fileHash, final HttpServletResponse response) throws IOException {
        previewControllerHelper.streamPreviewFile(response, fileHash);
    }

    @GetMapping("/file/thumbnail/{fileHash}")
    public void streamThumbnailFile(@PathVariable("fileHash") final String fileHash, final HttpServletResponse response) throws IOException {
        previewControllerHelper.streamThumbnailFile(response, fileHash);
    }

}
