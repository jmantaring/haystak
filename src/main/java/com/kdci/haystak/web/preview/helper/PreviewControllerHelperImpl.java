package com.kdci.haystak.web.preview.helper;

import com.kdci.haystak.service.filestorage.PreviewFileStorageService;
import com.kdci.haystak.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;

@Component
public class PreviewControllerHelperImpl implements PreviewControllerHelper {

    private static final String CONTENT_TYPE = "image/png";

    private PreviewFileStorageService previewFileStorageService;

    @Autowired
    public void setPreviewFileStorageService(PreviewFileStorageService previewFileStorageService) {
        this.previewFileStorageService = previewFileStorageService;
    }

    @Override
    public void streamPreviewFile(final HttpServletResponse response, final String fileHash) throws IOException {

        FileInputStream inputStream = previewFileStorageService.getPreviewStream(fileHash);

        WebUtils.stream(fileHash, inputStream, inputStream.getChannel().size(), response);
    }

    @Override
    public void streamThumbnailFile(final HttpServletResponse response, final String fileHash) throws IOException {
        FileInputStream inputStream = previewFileStorageService.getPreviewStream(fileHash);

        WebUtils.stream(fileHash, inputStream, inputStream.getChannel().size(), response);
    }

}
