package com.kdci.haystak.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.kdci.haystak.exception.GeneralException;

public abstract class AbstractAjaxController {
	
	/**
	 * General Handler of exception of the project
	 * 
	 * @param exception
	 * @return
	 */
	@ExceptionHandler(GeneralException.class)
	public ResponseEntity<?> handelException(GeneralException exception) {
		// TODO: Might need to do some other details in the future just not message and status
		return new ResponseEntity<Object>(exception.getMessageToUser(), exception.getHttpStatus());
	}

}
