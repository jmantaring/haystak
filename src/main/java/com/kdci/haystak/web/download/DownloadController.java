package com.kdci.haystak.web.download;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kdci.haystak.web.download.helper.DownloadControllerHelper;

@Controller
@RequestMapping(value = "/download/")
public class DownloadController {
	
	private DownloadControllerHelper downloadControllerHelper;
	
	@Resource
	public void setDownloadControllerHelper(DownloadControllerHelper downloadControllerHelper) {
		this.downloadControllerHelper = downloadControllerHelper;
	}

	@GetMapping("file/{fileId:\\d+}")
	public void downloadFile(@PathVariable("fileId") final String fileId, final HttpServletResponse response) {
		downloadControllerHelper.performDownload(response, fileId);
	}
	
}
