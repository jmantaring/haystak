package com.kdci.haystak.web.download.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.kdci.haystak.domain.download.DownloadCache;
import com.kdci.haystak.exception.GeneralException;
import com.kdci.haystak.service.DownloadCacheService;
import com.kdci.haystak.service.filestorage.FileStorageService;
import com.kdci.haystak.util.WebUtils;

@Component
public class DownloadControllerHelperImpl implements DownloadControllerHelper {

	private FileStorageService fileStorageService;
	
	private DownloadCacheService downloadCacheService;
	
	@Resource
	public void setFileStorageService(FileStorageService fileStorageService) {
		this.fileStorageService = fileStorageService;
	}
	
	@Resource
	public void setDownloadCacheService(DownloadCacheService downloadCacheService) {
		this.downloadCacheService = downloadCacheService;
	}
	
	@Override
	public void performDownload(final HttpServletResponse response, final String fileId) {

		// TODO: have to break down the logic and move to proper services
		
		InputStream inputStream = null;
		
		// TODO: use try resource java 8
		
		try {
			
			final DownloadCache downloadCache = downloadCacheService.findByFileId(fileId);
			
			if (Objects.isNull(downloadCache)) {
				throw new GeneralException("downloadCache is null.", HttpStatus.NOT_FOUND, "File not found."); // TODO: proper exception
			}
			
			if (!downloadCache.isReadyForDownload()) {
				throw new GeneralException("downloadCache.isReadyForDownload() is false.", HttpStatus.FORBIDDEN, "File is not ready for download."); // TODO: proper exception
			}
			
			inputStream = fileStorageService.getInputStream(downloadCache);
			
			WebUtils.stream(downloadCache.getFullFileName(), inputStream, downloadCache.getSize(), response);

		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

	}

}
