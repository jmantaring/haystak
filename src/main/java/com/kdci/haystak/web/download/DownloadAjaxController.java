package com.kdci.haystak.web.download;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kdci.haystak.helper.DownloadAjaxControllerHelper;
import com.kdci.haystak.web.AbstractAjaxController;

@Controller
@RequestMapping(value = "/download/setup")
public class DownloadAjaxController extends AbstractAjaxController {

	private DownloadAjaxControllerHelper downloadAjaxControllerHelper;

	@Autowired
	public void setDownloadAjaxControllerHelper(DownloadAjaxControllerHelper downloadAjaxControllerHelper) {
		this.downloadAjaxControllerHelper = downloadAjaxControllerHelper;
	}

	@GetMapping("file/{fileId}")
	@ResponseBody
	public Object generateDownloadUrl(@PathVariable("fileId") String fileId) {
		return downloadAjaxControllerHelper.generateDownloadUrl(fileId);
	}

}
