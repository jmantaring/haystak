package com.kdci.haystak.web.download.helper;

import javax.servlet.http.HttpServletResponse;

public interface DownloadControllerHelper {

	void performDownload(HttpServletResponse response, final String fileId);
	
}
