package com.kdci.haystak.service;

public interface EncapsaDownloadService {

    String generateFileDownloadUrl(String fileId);

    void fetchData(String fileId);

}
