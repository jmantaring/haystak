package com.kdci.haystak.service.filestorage.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.kdci.haystak.domain.download.DownloadCache;
import com.kdci.haystak.service.filestorage.FileStorageService;

@Service
public class FileStorageServiceImpl implements FileStorageService {

	private String storageLocation;
	
	@Value("${application.download.file.location}")
	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}
	
	@Override
	public InputStream getInputStream(final DownloadCache downloadCache) throws IOException {

		final File file = new File(storageLocation, downloadCache.getFileId());
		
		final FileInputStream inputStream = new FileInputStream(file);
		
		return inputStream;
	}

}
