package com.kdci.haystak.service.filestorage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public interface PreviewFileStorageService {

    FileInputStream getThumbnailStream(String fileHash) throws FileNotFoundException;

    FileInputStream getPreviewStream(String fileHash) throws FileNotFoundException;

}
