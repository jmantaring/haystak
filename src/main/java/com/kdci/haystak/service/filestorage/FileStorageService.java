package com.kdci.haystak.service.filestorage;

import java.io.IOException;
import java.io.InputStream;

import com.kdci.haystak.domain.download.DownloadCache;

public interface FileStorageService {
	
	public InputStream getInputStream(final DownloadCache downloadCache) throws IOException;
	
}
