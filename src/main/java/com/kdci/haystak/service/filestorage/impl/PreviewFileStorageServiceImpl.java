package com.kdci.haystak.service.filestorage.impl;

import com.kdci.haystak.service.filestorage.PreviewFileStorageService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@Service
public class PreviewFileStorageServiceImpl implements PreviewFileStorageService {

    private String thumbnailStorage;

    private String previewStorage;

    @Value("${application.thumbnail.file.location}")
    public void setThumbnailStorage(String thumbnailStorage) {
        this.thumbnailStorage = thumbnailStorage;
    }

    @Value("${application.preview.file.location}")
    public void setPreviewStorage(String previewStorage) {
        this.previewStorage = previewStorage;
    }

    @Override
    public FileInputStream getThumbnailStream(String fileHash) throws FileNotFoundException {

        final File file = new File(thumbnailStorage, fileHash);

        final FileInputStream inputStream = new FileInputStream(file);

        return inputStream;

    }

    @Override
    public FileInputStream getPreviewStream(String fileHash) throws FileNotFoundException {

        final File file = new File(previewStorage, fileHash);

        final FileInputStream inputStream = new FileInputStream(file);

        return inputStream;

    }
}
