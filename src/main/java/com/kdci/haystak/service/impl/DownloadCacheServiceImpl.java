package com.kdci.haystak.service.impl;

import com.kdci.haystak.bean.EncapsaRowData;
import com.kdci.haystak.domain.download.DownloadCache;
import com.kdci.haystak.repository.DownloadCacheRepository;
import com.kdci.haystak.service.DownloadCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("downloadCacheService")
public class DownloadCacheServiceImpl implements DownloadCacheService {

    private DownloadCacheRepository downloadCacheRepository;

    @Autowired
    public void setDownloadCacheRepository(DownloadCacheRepository downloadCacheRepository) {
        this.downloadCacheRepository = downloadCacheRepository;
    }

    @Override
    @Transactional
    public DownloadCache create(String fileId, boolean isReadyForDownload) {

        DownloadCache downloadCache = new DownloadCache(fileId, isReadyForDownload);

        return downloadCacheRepository.create(downloadCache);

    }

    @Override
    @Transactional(readOnly = true)
    public boolean isFileIdExisting(String fileId) {
        return downloadCacheRepository.isFileIdExisting(fileId);
    }

    @Override
    @Transactional(readOnly = true)
    public DownloadCache findByFileId(String fileId) {
        return downloadCacheRepository.findByFileId(fileId);
    }

    @Override
    @Transactional
    public DownloadCache update(DownloadCache downloadCache) {
        return downloadCacheRepository.update(downloadCache);
    }

    @Override
    @Transactional
    public DownloadCache setCacheReadyForDownload(String fileId, EncapsaRowData encapsaRowData) {
        DownloadCache downloadCache = downloadCacheRepository.findByFileId(fileId);

        downloadCache.setReadyForDownload(true);

        downloadCache.setFullFileName(encapsaRowData.getFULLFILENAME());

        downloadCache.setContentType(encapsaRowData.getCONTENTTYPE());

        downloadCache.setSize(Long.parseLong(encapsaRowData.getFILESIZE()));

        return downloadCacheRepository.update(downloadCache);
    }

}
