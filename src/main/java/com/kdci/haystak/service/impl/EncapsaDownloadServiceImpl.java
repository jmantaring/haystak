package com.kdci.haystak.service.impl;

import com.kdci.haystak.EncapsaAsyncFetchConfig;
import com.kdci.haystak.bean.EncapsaRowData;
import com.kdci.haystak.exception.GeneralException;
import com.kdci.haystak.helper.EncapsaDownloadResultParser;
import com.kdci.haystak.service.DownloadCacheService;
import com.kdci.haystak.service.EncapsaDownloadService;
import com.kdci.haystak.util.Base64Utils;
import com.kdci.haystak.util.PathUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@Service("encapsaDownloadService")
public class EncapsaDownloadServiceImpl implements EncapsaDownloadService {

    private static final Logger logger = LoggerFactory.getLogger(EncapsaDownloadServiceImpl.class);

    private static final String DOWNLOAD_PATH = "download/file";

    private static final String EMPTY_JSON_STRING = "{}";

    private DownloadCacheService downloadCacheService;

    private EncapsaDownloadResultParser encapsaDownloadResultParser;

    private String applicationBaseUrl;

    private String encapsaDownloadUrl;

    private String downloadLocation;

    @Autowired
    public void setDownloadCacheService(DownloadCacheService downloadCacheService) {
        this.downloadCacheService = downloadCacheService;
    }

    @Autowired
    public void setEncapsaDownloadResultParser(EncapsaDownloadResultParser encapsaDownloadResultParser) {
        this.encapsaDownloadResultParser = encapsaDownloadResultParser;
    }

    @Value("${application.base.url}")
    public void setApplicationBaseUrl(String applicationBaseUrl) {
        this.applicationBaseUrl = applicationBaseUrl;
    }

    @Value("${application.encapsa.file.download.url}")
    public void setEncapsaDownloadUrl(String encapsaDownloadUrl) {
        this.encapsaDownloadUrl = encapsaDownloadUrl;
    }

    @Value("${application.download.file.location}")
    public void setDownloadLocation(String downloadLocation) {
        this.downloadLocation = downloadLocation;
    }

    @Override
    @Transactional
    public String generateFileDownloadUrl(String fileId) {

        if (downloadCacheService.isFileIdExisting(fileId)) {
            return generateDownloadUrl(fileId);
        }  else {

            downloadCacheService.create(fileId, false);

            return generateDownloadUrl(fileId);
        }

    }

    // TODO: Still have to REFACTOR THIS CODE.
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
//    @Async(EncapsaAsyncFetchConfig.ENCAPSA_FETCH_EXECUTOR) TODO: TEMPORARY DISABLE ASYNC UNTIL FUTHER SPECS IS GIVEN
    public void fetchData(String fileId) {

        // TODO need retry functionality for server hiccups 10, 50, 100 interval

        logger.info("Start fetching data from encapsa");

        RestTemplate restTemplate = new RestTemplate();

        final String downloadUrl = PathUtils.combinePaths(encapsaDownloadUrl, fileId);

        logger.info("Accessing encapsa with url {}", downloadUrl);

        String result = EMPTY_JSON_STRING;

        try {
            result = restTemplate.getForObject(downloadUrl, String.class);
        } catch (RestClientException e) {
            e.printStackTrace();
            throw new GeneralException("HTTP error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        logger.debug("Result {}", result);

        final String sanitizedString = sanitize(result);

        final EncapsaRowData encapsaRowData = encapsaDownloadResultParser.parseJson(sanitizedString);

        if (Objects.isNull(encapsaRowData)) {
            logger.error("Unable to parse encapsa data");
            throw new GeneralException("Unable to parse encapsa data", HttpStatus.FAILED_DEPENDENCY);
        }

        final String fileData = encapsaRowData.getFILEDATA();

        if (StringUtils.isEmpty(fileData)) {
            logger.error("FILEDATA is empty. Aborting!");
            throw new GeneralException("FILEDATA is empty", HttpStatus.FAILED_DEPENDENCY);
        }

        final byte[] bytes = Base64Utils.convertToArray(fileData);

        createFile(fileId, bytes);

        downloadCacheService.setCacheReadyForDownload(fileId, encapsaRowData);

        logger.info("Done fetching and updating records");

    }
    
    private void createFile(String fileId, byte[] bytes) {
        InputStream is = new BufferedInputStream(new ByteArrayInputStream(bytes));

        Path path = Paths.get(downloadLocation, fileId);

        File file = path.toFile();

        OutputStream os = null;

        // TODO: use java 8 try resource
        try {

            os = new BufferedOutputStream(new FileOutputStream(file));

            // save the FILEDATA locally
            IOUtils.copy(is, os);

            os.flush();

            IOUtils.closeQuietly(os);
            IOUtils.closeQuietly(is);

        } catch (IOException e) {
            e.printStackTrace();

            IOUtils.closeQuietly(os);
            IOUtils.closeQuietly(is);
        }

    }

    private String sanitize(String str) {

        final int start = 1;

        final int end = str.length() - 1;

        String sanitized = StringUtils.substring(str, start, end);

        sanitized = StringEscapeUtils.unescapeJson(sanitized);

        return sanitized;
    }

    private String generateDownloadUrl(String path) {
        return PathUtils.combinePaths(applicationBaseUrl, DOWNLOAD_PATH, path);
    }

}
