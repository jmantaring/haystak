package com.kdci.haystak.service;

import com.kdci.haystak.bean.EncapsaRowData;
import com.kdci.haystak.domain.download.DownloadCache;

public interface DownloadCacheService {

    DownloadCache create(String fileId, boolean isReadyForDownload);

    boolean isFileIdExisting(String fileId);

    DownloadCache findByFileId(String fileId);

    DownloadCache update(DownloadCache downloadCache);

    DownloadCache setCacheReadyForDownload(String fileId, EncapsaRowData encapsaRowData);
}
