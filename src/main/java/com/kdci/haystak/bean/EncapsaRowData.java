package com.kdci.haystak.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Case is ignored by the mapper, should we change this to camel case?
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EncapsaRowData {

    private String CONTENTTYPE;

    private String DATEADDED;

    private String DATELASTMODIFIED;

    private String FILEDATA;

    private String FILEEXT;

    private String FILEHASH;

    private String FILEID;

    private String FILENAME;

    private String FILESIZE;

    private String FULLFILENAME;

    private String METADATA;

    private String OWNERID;

    private String OWNERNAME;

    private String TIMEADDED;

    public String getCONTENTTYPE() {
        return CONTENTTYPE;
    }

    public void setCONTENTTYPE(String CONTENTTYPE) {
        this.CONTENTTYPE = CONTENTTYPE;
    }

    public String getDATEADDED() {
        return DATEADDED;
    }

    public void setDATEADDED(String DATEADDED) {
        this.DATEADDED = DATEADDED;
    }

    public String getDATELASTMODIFIED() {
        return DATELASTMODIFIED;
    }

    public void setDATELASTMODIFIED(String DATELASTMODIFIED) {
        this.DATELASTMODIFIED = DATELASTMODIFIED;
    }

    public String getFILEDATA() {
        return FILEDATA;
    }

    public void setFILEDATA(String FILEDATA) {
        this.FILEDATA = FILEDATA;
    }

    public String getFILEEXT() {
        return FILEEXT;
    }

    public void setFILEEXT(String FILEEXT) {
        this.FILEEXT = FILEEXT;
    }

    public String getFILEHASH() {
        return FILEHASH;
    }

    public void setFILEHASH(String FILEHASH) {
        this.FILEHASH = FILEHASH;
    }

    public String getFILEID() {
        return FILEID;
    }

    public void setFILEID(String FILEID) {
        this.FILEID = FILEID;
    }

    public String getFILENAME() {
        return FILENAME;
    }

    public void setFILENAME(String FILENAME) {
        this.FILENAME = FILENAME;
    }

    public String getFILESIZE() {
        return FILESIZE;
    }

    public void setFILESIZE(String FILESIZE) {
        this.FILESIZE = FILESIZE;
    }

    public String getFULLFILENAME() {
        return FULLFILENAME;
    }

    public void setFULLFILENAME(String FULLFILENAME) {
        this.FULLFILENAME = FULLFILENAME;
    }

    public String getMETADATA() {
        return METADATA;
    }

    public void setMETADATA(String METADATA) {
        this.METADATA = METADATA;
    }

    public String getOWNERID() {
        return OWNERID;
    }

    public void setOWNERID(String OWNERID) {
        this.OWNERID = OWNERID;
    }

    public String getOWNERNAME() {
        return OWNERNAME;
    }

    public void setOWNERNAME(String OWNERNAME) {
        this.OWNERNAME = OWNERNAME;
    }

    public String getTIMEADDED() {
        return TIMEADDED;
    }

    public void setTIMEADDED(String TIMEADDED) {
        this.TIMEADDED = TIMEADDED;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
