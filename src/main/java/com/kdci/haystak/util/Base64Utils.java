package com.kdci.haystak.util;

import java.util.Base64;

// TODO: DECIDE IF WE USE THIS OR org.springframework.util.Base64Utils
// Difference is that spring Base64Utils getting bytes of the string from StandardCharsets.UTF_8
public class Base64Utils {

    public static byte[] convertToArray(String b64String) {
        Base64.Decoder decoder = Base64.getDecoder();

        return decoder.decode(b64String);
    }

}
