package com.kdci.haystak.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;

public final class WebUtils {
	
	public static final String OCTET_STREAM_CONTENT_TYPE = "application/octet-stream";
	
	public static final String RESPONSE_HEADER_CONTENT_LENGTH = "Content-Length";
	
	public static final String RESPONSE_HEADER_CONTENT_DISPOSITION = "Content-Disposition";
	
	private WebUtils() {
		super();
	}
	
	/**
	 * Write InputStream to the HttpServletResponse
	 * 
	 * @param rawFileName the raw file name
	 * @param inputStream the input stream of the file
	 * @param size the size of the file
	 * @param response the HttpServletResponse to write on
	 * @throws IOException
	 */
	public static void stream(String rawFileName, InputStream inputStream, long size, HttpServletResponse response) throws IOException {
		WebUtils.stream(rawFileName, inputStream, size, OCTET_STREAM_CONTENT_TYPE, response);
	}

	/**
	 * Write InputStream to the HttpServletResponse
	 * 
	 * @param rawFileName the raw file name
	 * @param inputStream the input stream of the file
	 * @param size the size of the file
	 * @param contentType the contentType of the file (i.e "application/vnd.ms-excel" for excel files)
	 * @param response the HttpServletResponse to write on
	 * @throws IOException
	 */
	public static void stream(String rawFileName, InputStream inputStream, long size, String contentType, HttpServletResponse response) throws IOException {

		response.setContentType(contentType);
		response.addHeader(RESPONSE_HEADER_CONTENT_LENGTH, Long.toString(size));
		response.addHeader(RESPONSE_HEADER_CONTENT_DISPOSITION, new StringBuilder(rawFileName.length() + 40).append("attachment; filename=\"").append(rawFileName).append("\"").toString());

		OutputStream outputStream = response.getOutputStream();

		IOUtils.copy(inputStream, outputStream);
	}

}
