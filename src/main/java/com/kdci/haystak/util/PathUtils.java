package com.kdci.haystak.util;

public final class PathUtils {

    private static final String FORWARD_SLASH = "/";

    public static final String combinePaths(String contextPath, String... paths) {

        StringBuilder sb = new StringBuilder(128);

        sb.append(contextPath);

        for (String path: paths) {
            sb.append(FORWARD_SLASH);
            sb.append(path);
        }

        return sb.toString();
    }

}
