package com.kdci.haystak.util;

import java.util.List;

import javax.persistence.TypedQuery;

import org.apache.commons.collections4.CollectionUtils;

public final class RepositoryUtils {

	private RepositoryUtils() {
		super();
	}
	
	public static <T> T getSingleResult(final TypedQuery<T> typedQuery) {
		
		final List<T> results = typedQuery.getResultList();
		
		return CollectionUtils.isNotEmpty(results) ? results.get(0) : null;
	}
	
}
