package com.kdci.haystak.repository;

import javax.persistence.EntityManager;

public interface Repository<T> {

	EntityManager getEntityManager();

	T find(final Long id);
	
	T create(final T domain);

    T update(T domain);
}
