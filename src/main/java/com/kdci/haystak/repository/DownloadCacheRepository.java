package com.kdci.haystak.repository;

import com.kdci.haystak.domain.download.DownloadCache;

public interface DownloadCacheRepository extends Repository<DownloadCache> {
    boolean isFileIdExisting(String fileId);

    DownloadCache findByFileId(String fileId);
}
