package com.kdci.haystak.repository.impl;

import com.kdci.haystak.domain.download.DownloadCache;
import com.kdci.haystak.repository.DownloadCacheRepository;
import com.kdci.haystak.util.RepositoryUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.Objects;

@Repository("downloadCacheRepository")
public class DownloadCacheRepositoryImpl extends AbstractRepository<DownloadCache> implements DownloadCacheRepository {

    public DownloadCacheRepositoryImpl() {
        super(DownloadCache.class);
    }

    @Override
    public boolean isFileIdExisting(String fileId) {

        final String qlString = "SELECT dc FROM DownloadCache dc WHERE dc.fileId = :fileId";

        TypedQuery<DownloadCache> query = getEntityManager().createQuery(qlString, DownloadCache.class);

        query.setParameter("fileId", fileId);

        query.setMaxResults(1);

        DownloadCache result = RepositoryUtils.getSingleResult(query);

        return Objects.nonNull(result);
    }

    @Override
    public DownloadCache findByFileId(String fileId) {

        final String qlString = "SELECT dc FROM DownloadCache dc WHERE dc.fileId = :fileId";

        TypedQuery<DownloadCache> query = getEntityManager().createQuery(qlString, DownloadCache.class);

        query.setParameter("fileId", fileId);

        query.setMaxResults(1);

        return RepositoryUtils.getSingleResult(query);
    }
}
