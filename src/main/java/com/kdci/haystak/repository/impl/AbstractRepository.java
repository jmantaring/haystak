package com.kdci.haystak.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.kdci.haystak.repository.Repository;
import com.kdci.haystak.util.RepositoryUtils;

public class AbstractRepository<T> implements Repository<T> {

	private Class<T> domainClass;
	
	private EntityManager entityManager;
	
	@PersistenceContext
	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public AbstractRepository(final Class<T> domainClass) {
		super();
		this.domainClass = domainClass;
	}

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public T find(final Long id) {

		final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		
		final CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(this.domainClass);
		
		final Root<T> root = criteriaQuery.from(domainClass);
		
		criteriaQuery.where(criteriaBuilder.equal(root.get("id"), id));
		
		criteriaQuery.select(root);
		
		final TypedQuery<T> query = entityManager.createQuery(criteriaQuery);
		
		return RepositoryUtils.getSingleResult(query);
	}

	@Override
	public T create(final T domain) {

		entityManager.persist(domain);
		
		return domain;
	}

	@Override
	public T update(final T domain) {

		T result = entityManager.merge(domain);

		entityManager.flush();

		return result;
	}

}
