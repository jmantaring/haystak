package com.kdci.haystak.domain.download;

import javax.persistence.Entity;

import com.kdci.haystak.domain.AbstractDomain;

@Entity
public class DownloadCache extends AbstractDomain {
	
	// TODO: should we set a limit for this one? or extend it to Long text?
	// TODO: make this unique
	private String fileId;
	
	private boolean isReadyForDownload;

	private String fullFileName;

	private String contentType;

	private long size;

	public DownloadCache() {
		super();
	}

	public DownloadCache(final String fileId, final boolean isReadyForDownload) {
		super();
		this.fileId = fileId;
		this.isReadyForDownload = isReadyForDownload;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public boolean isReadyForDownload() {
		return isReadyForDownload;
	}

	public void setReadyForDownload(boolean isReadyForDownload) {
		this.isReadyForDownload = isReadyForDownload;
	}

	public String getFullFileName() {
		return fullFileName;
	}

	public void setFullFileName(String fullFileName) {
		this.fullFileName = fullFileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DownloadCache other = (DownloadCache) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
