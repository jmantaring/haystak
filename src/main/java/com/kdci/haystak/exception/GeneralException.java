package com.kdci.haystak.exception;

import org.springframework.http.HttpStatus;

public class GeneralException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1084979999493552054L;
	
	private final HttpStatus status;
	
	private final String messageToUser;
	
	public GeneralException() {
		super("Internal Server Error.");
		this.status = HttpStatus.INTERNAL_SERVER_ERROR;
		this.messageToUser = "Internal Server Error.";
	}
	
	/**
	 * Creating General Exception with same message to Users and Developers
	 * 
	 * @param message the message to display to users and developer
	 * @param status the status of the error
	 */
	public GeneralException(final String message, final HttpStatus status) {
		super(message);
		this.status = status;
		this.messageToUser = message;
	}
	
	/**
	 * Create General Exception where message to Users and Developers are different
	 * 
	 * @param message the message to display to users
	 * @param status the status of the error
	 * @param messageToUser the message to display to developers
	 */
	public GeneralException(final String message, final HttpStatus status, final String messageToUser) {
		super(message);
		this.status = status;
		this.messageToUser = messageToUser;
	}
	
	public HttpStatus getHttpStatus() {
		return status;
	}

	public String getMessageToUser() {
		return messageToUser;
	}

}
