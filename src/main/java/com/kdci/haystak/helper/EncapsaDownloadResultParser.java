package com.kdci.haystak.helper;

import com.kdci.haystak.bean.EncapsaRowData;

import java.io.InputStream;

public interface EncapsaDownloadResultParser {

    EncapsaRowData parseJson(String json);

    EncapsaRowData parseJSONStream(InputStream inputStream);
}
