package com.kdci.haystak.helper;

public interface DownloadAjaxControllerHelper {
    String generateDownloadUrl(String fileId);
}
