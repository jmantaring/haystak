package com.kdci.haystak.helper.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kdci.haystak.bean.EncapsaRowData;
import com.kdci.haystak.exception.GeneralException;
import com.kdci.haystak.helper.EncapsaDownloadResultParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.DataBinder;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO implement to use jackson only
 */
@Component("encapsaDownloadResultParser")
public class EncapsaDownloadResultParserImpl implements EncapsaDownloadResultParser {

    private static final Logger logger = LoggerFactory.getLogger(EncapsaDownloadResultParserImpl.class);

    private static final String ENCAPSA_DECLARATION_ROW = "Row";

    public static final int FIRST_ENTRY = 0;

    @Override
    public EncapsaRowData parseJson(String json) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        mapper.configure(MapperFeature.AUTO_DETECT_SETTERS, true);

        mapper.configure(com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);

        try {
            Map<String, Object> map = mapper.readValue(json, new TypeReference<Map<String, Object>>(){});

            ArrayList<Object> objects = new ArrayList<>();
            objects.addAll((Collection<?>) map.get("Row"));

            logger.info("list {}", objects.get(0));

            EncapsaRowData rowData = new EncapsaRowData();

            DataBinder binder = new DataBinder(rowData);
            binder.bind(new MutablePropertyValues(new HashMap((Map) objects.get(0))));

            return rowData;
        } catch (IOException e) {

            logger.error("Failed to parse encapsa data");

            // TODO create proper exception
            throw new GeneralException("Failed to parse EnCapsa data", HttpStatus.PRECONDITION_FAILED);
        }

     // TODO: have to remove GSON and use spring and jackson for data binding if possible
//        JsonParser jsonParser = new JsonParser();
//
//        JsonElement element = jsonParser.parse(json);
//
//        JsonObject jsonObject = element.getAsJsonObject();
//
//        final String rowJsonString = jsonObject.get(ENCAPSA_DECLARATION_ROW).getAsJsonArray().get(FIRST_ENTRY).toString();
//
//        try {
//            return mapper.readValue(rowJsonString, EncapsaRowData.class);
//        } catch (IOException e) {
//
//            logger.error("Failed to parse {}", rowJsonString);
//
//            // TODO create proper exception
//            throw new GeneralException("Failed to parse EnCapsa data", HttpStatus.PRECONDITION_FAILED);
//        }

    }

    @Override
    public EncapsaRowData parseJSONStream(InputStream inputStream) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        mapper.configure(MapperFeature.AUTO_DETECT_SETTERS, true);

        String rowJsonString = null;

        try {
        	// TODO: have to remove GSON and use spring and jackson for data binding if possible
            JsonParser jsonParser = new JsonParser();

            JsonObject jsonObject = (JsonObject) jsonParser.parse(new InputStreamReader(inputStream, "UTF-8"));

            rowJsonString = jsonObject.get(ENCAPSA_DECLARATION_ROW).getAsJsonArray().get(FIRST_ENTRY).toString();

            return mapper.readValue(rowJsonString, EncapsaRowData.class);
        } catch (IOException e) {

            logger.error("Failed to parse {}", rowJsonString);

            // TODO create proper exception
            throw new GeneralException("Failed to parse EnCapsa data", HttpStatus.PRECONDITION_FAILED);
        }

    }

}
