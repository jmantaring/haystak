package com.kdci.haystak.helper.impl;

import com.kdci.haystak.helper.DownloadAjaxControllerHelper;
import com.kdci.haystak.service.EncapsaDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DownloadAjaxControllerHelperImpl implements DownloadAjaxControllerHelper {

    private EncapsaDownloadService encapsaDownloadService;

    @Autowired
    public void setEncapsaDownloadService(EncapsaDownloadService encapsaDownloadService) {
        this.encapsaDownloadService = encapsaDownloadService;
    }

    @Override
    public String generateDownloadUrl(String fileId) {

        String downloadUrl = encapsaDownloadService.generateFileDownloadUrl(fileId);

        encapsaDownloadService.fetchData(fileId);

        return downloadUrl;

    }

}
