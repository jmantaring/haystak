package com.kdci.haystak;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.lang.reflect.Method;
import java.util.concurrent.Executor;

@Configuration
@EnableAsync
public class EncapsaAsyncFetchConfig implements AsyncUncaughtExceptionHandler, AsyncConfigurer {

    public static final String ENCAPSA_FETCH_EXECUTOR = "encapsaFetchExecutor";

    private static final Logger logger = LoggerFactory.getLogger(EncapsaAsyncFetchConfig.class);

    private ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();

    @Bean(ENCAPSA_FETCH_EXECUTOR)
    public Executor asyncExecutor() {
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix(ENCAPSA_FETCH_EXECUTOR);
        executor.initialize();
        return executor;
    }

    @Override
    public void handleUncaughtException(Throwable ex, Method method, Object... params) {
        logger.warn("Handling uncaught exception for {} {}", ex.getMessage(), method);

        // TODO handler for letting caller to know the method failed
        // WARNING DO NOT invoke method here

        ex.printStackTrace();
    }

    @Override
    public Executor getAsyncExecutor() {
        return this.executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return this;
    }

}
