package com.kdci.haystak.factory;

import com.kdci.haystak.bean.FileHashDataBean;
import com.kdci.haystak.util.PathUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FileHashDataBeanFactoryImpl implements FileHashDataBeanFactory {

    private static final String THUMB_SUFFIX = "_thumb.png";

    private static final String PREVIEW_SUFFIX = "_preview.png";

    private String applicationUrl;

    @Value("${application.base.url}")
    public void setApplicationUrl(String applicationUrl) {
        this.applicationUrl = applicationUrl;
    }

    @Override
    public FileHashDataBean create(String fileHash) {

        final FileHashDataBean bean = new FileHashDataBean();

        bean.setFileHash(fileHash);

        bean.setPreviewUrl(PathUtils.combinePaths(applicationUrl, "preview/file/preview", fileHash + PREVIEW_SUFFIX));

        bean.setThumbUrl(PathUtils.combinePaths(applicationUrl, "preview/file/thumbnail", fileHash + THUMB_SUFFIX));

        return bean;

    }

}
