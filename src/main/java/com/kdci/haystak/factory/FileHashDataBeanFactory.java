package com.kdci.haystak.factory;

import com.kdci.haystak.bean.FileHashDataBean;

public interface FileHashDataBeanFactory {

    FileHashDataBean create(String fileHash);
}
